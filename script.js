let name = prompt("Введіть ваше ім'я:");
let age = parseInt(prompt("Введіть ваш вік:"));

while (!name || isNaN(age)) {
  name = prompt("Будь ласка, введіть ваше ім'я:");
  age = parseInt(prompt("Будь ласка, введіть ваш вік:"));
}

if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  const confirmation = confirm("Are you sure you want to continue?");
  if (confirmation) {
    alert("Welcome, " + name + "!");
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert("Welcome, " + name + "!");
}
